<?php
/**
* Plugin Name: RadioCo Import
* Plugin URI: 
* Description: gets and stores Post (Meta) from https://co.radiocorax.de/.
* Version: 1.5.1
* Author: Ronny Rohland (pre: Marian Mortell)
* Author URI: http://derron.de
* License: GPL3
*/


//Den Navigationsreiter einf�gen

add_action('admin_menu', 'myplugin_admin_menu');

function myplugin_admin_menu() {
  $page_title = 'Plugin for importing RadioCo programmes into Wordpress';
  $menu_title = 'RadioCo Import';
  $capability = 'manage_options';
  $menu_slug = 'myplugin-settings';
  $function = 'myplugin_settings';
  add_options_page($page_title, $menu_title, $capability, $menu_slug, $function);
}

function myplugin_settings() {
  if (!current_user_can('manage_options')) {
    wp_die('You do not have sufficient permissions to access this page.');
  }
  MyCronAction();
}


function search_array($products, $field, $value)
{
  foreach($products as $key => $product)
  {
    if ( $product[$field] === $value ){
      $sendungen[] = $key;
    } 
  }
  return $sendungen;
}

function MyCronAction() {

  include('progressbar.class.php');

  $times_url = "https://co.radiocorax.de/api/2/transmissions";
  $times_json = file_get_contents($times_url);
  $times_data = json_decode($times_json, TRUE);

  $url = "https://co.radiocorax.de/api/2/programmes";
  $json = file_get_contents($url);
  $data = json_decode($json, TRUE);

  $cunt = count($data);
  $i=0;
  echo '<h1>RadioCo Import (json)</h1>
  <p>RadioCo stats: <br/>
    '.$times_url.': '.count($times_data).' Sendezeiten<br/>
    '.$url.': '.$cunt.' Programme<br/><br/>
  </p>'; 
  ?>

  <form  method="post" enctype="multipart/form-data">
    <input type="submit" id="submit" name="submit" value="Programme aktualisieren"/>
  </form>

  <?php

  $myprogressbar1 = new progressbar(0, $cunt, 200, 20);     

  if(isset($_POST["submit"]) || defined( 'DOING_CRON' )){ 

    $programmCount = 0;

      //for($k = $programmCount; $k <= $programmCount+ 20; $k++){
    for($k = $programmCount; $k <= $cunt; $k++){

      $i++;

      $field = $data[$k];

      $title = $field['name'];
      $sum = $field['synopsis'];
      $thump = $field['photo'];
      $cat =  $field['category'];
      $language = $field['language'];
      $slug =  $field['slug'];
      $runtime = $field['runtime'];

      $found_entrys = search_array($times_data, "slug", $slug);
      $found_entrys_count = count($found_entrys);

      $echo_text = "";

      if ($found_entrys_count == 0) $echo_text .= "<font color='#bf0000'><strong>";
      $echo_text .= $k.": ".$title.": ";

      if($found_entrys_count > 0){

        $post = array(
          'post_title' => $title,
          'post_content' => $sum,
          'post_name' => $slug,
          'post_status' => 'publish',
          'post_type' => 'post',
          'comment_status' => 'closed',
          'post_category' => array(6)
          );

        $post_check = get_page_by_path( $slug, 'OBJECT', 'post' );
        $last_timestamp = get_post_meta( $post_id, 'timestamp', true );


        //var_dump($last_timestamp);

        
        //$echo_text .= $found_entrys; 
        $start = "";
        $last_timestamp = 0;

        foreach ($found_entrys as $entry) {

          $start_tmp = $times_data[$entry]['start'];
          $start_tmp_timestamp = strtotime($start_tmp);

          if ($start_tmp_timestamp > (time()-18000)){

          //$end_tmp = $times_data[$entry]['end'];

            $echo_text .= "<font color='#00bf00'><strong>".$start_tmp."</strong></font> | ";


            if ($start_tmp != "" && $start != "") $start .= "##";
            if ($start_tmp != "") $start .= $start_tmp;

            if ($start_tmp_timestamp > $last_timestamp) $last_timestamp = $start_tmp_timestamp;

          }else{
            $echo_text .= "<font color='#FFA500'><strong>".$start_tmp."</strong></font> | ";
          }

          //if ($end_tmp != "" && $end != "") $end .= "##";
          //if ($end_tmp != "") $end .= $end_tmp;
        }

        //var_dump($last_timestamp);
        //die();
        if($last_timestamp > 0){ 

          $echo_text .= "<font color='#00bf00'><strong>aktualisiert</strong></font>";

          if(NULL===$post_check){ 

            //die();

            $post_id = wp_insert_post($post);       

            update_post_meta( $post_id, 'laufzeit', $runtime );
            update_post_meta( $post_id, 'Kategorie', $cat );
            update_post_meta( $post_id, 'Sprache', $language );
            update_post_meta( $post_id, 'start', $start );
            update_post_meta( $post_id, 'timestamp', $last_timestamp );
            //delete_post_meta( $post_id, 'end', $end );

          }else{       

            $id = $post_check->ID;
            $update_post = array (
              'ID' =>	$id,
              'post_title' => $title,
              'post_content' => $sum,
              'post_type' => "post",
              'post_status' => "publish"
              );

            wp_update_post( $update_post );
            update_post_meta( $id, 'laufzeit', $runtime );
            update_post_meta( $id, 'Kategorie', $cat );
            update_post_meta( $id, 'Sprache', $language );
            update_post_meta( $id, 'start', $start );
            update_post_meta( $id, 'timestamp', $last_timestamp );
            //delete_post_meta( $id, 'end', $end );

          }
        }else{
          $echo_text .= "<font color='#bf0000'><strong>keine Sendezeiten in der Zukunft gefunden ...</strong></font>";
        }
      }else{
        $echo_text .= "keine Sendezeiten gefunden ...";
      }

      $myprogressbar1->step();  
      usleep(5000);    

      if ($i < 2) {
        $myprogressbar1->print_code();     
        echo '<br><strong>Folgende Programme wurden aktualisiert: </strong><br><br>';
      }


      if ($found_entrys_count == 0) $echo_text .= "</strong></font>";
      $echo_text .= '<br>'; 

      echo $echo_text;

    }

  }

  //

}


// create a scheduled event (if it does not exist already)
function cronstarter_activation() {
  if( !wp_next_scheduled( 'mycronjob' ) ) {  
   wp_schedule_event( time(), 'daily', 'mycronjob' );  
 }
}
// and make sure it's called whenever WordPress loads
add_action('wp', 'cronstarter_activation');


add_action('mycronjob', 'MyCronAction');